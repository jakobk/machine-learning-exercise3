package at.ac.tuwien.machinelearning;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import weka.classifiers.meta.AdaBoostM1;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;
import weka.gui.visualize.AttributePanel;
import weka.gui.visualize.ClassPanel;
import weka.gui.visualize.InstanceInfoFrame;
import weka.gui.visualize.LegendPanel;
import weka.gui.visualize.MatrixPanel;
import weka.gui.visualize.Plot2D;
import weka.gui.visualize.PlotData2D;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.Vector;

/**
 * Main for Exercise 3.
 *
 * @author Jakob Korherr
 */
public class Main {

    private static final Logger log = LoggerFactory.getLogger(Main.class);

    private static final String FILENAME = "exercise3/src/main/resources/iris/iris.data";

    public static void main(String[] args) throws Exception {
        // GUI
        MainFrame mainFrame = new MainFrame();
        mainFrame.setVisible(true);
    }

}
